<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Input;
use Zipper;
use DB;
use Artisan;
use File;

class PluginController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		//
		return view('plugin.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function check_plugin(Request $request) {
		$file = Input::file('plugin');
		$fileName = $file -> getClientOriginalName();
		$destinationPath = 'resources/plugins/';
		$file_path = $destinationPath . $fileName;
		$file -> move($destinationPath, $fileName);
		$str = file_get_contents('zip://' . $file_path . '#plugin.json');
		$plugin_details = json_decode($str, true);
		$plugin_icons = base64_encode(file_get_contents('zip://'.$file_path.'#'.$plugin_details['plugin']['icon']));

		
		$plugin_path = base64_encode($file_path);
		return view('plugin.install',compact('plugin_details','plugin_icons','plugin_path'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request) {

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		//
	}
	
	public function install(Request $request){
		$file_path = base64_decode($request->input('plugin_path'));
		$file_path = public_path()."/".$file_path;
		$zip = new \ZipArchive();
		$zip -> open($file_path);
		$zip_bool = zip_open($file_path);
		if ($zip_bool) {
			while ($zip_entry = zip_read($zip_bool)) {
				if (substr(strrchr(zip_entry_name($zip_entry), '/'), 1)) {
					$plugin = substr(strrchr(zip_entry_name($zip_entry), '/'), 1);
					$dir = explode('/', zip_entry_name($zip_entry));
 					array_pop($dir);
					$newpath = implode('/', $dir);					
					$dir = "";
				
					for ($i = 0; $i < $zip -> numFiles; $i++) {
						$path = zip_entry_name($zip_entry);
						$filename = $zip -> getNameIndex($i);
						$fileinfo = pathinfo($filename);

						if($newpath == "Controllers"){
							$dir = app_path()."/Http/Controllers/";
							copy("zip://".$file_path."#".$path, $dir.substr(strrchr(zip_entry_name($zip_entry), '/'), 1));
						}
						else if($newpath == "assets/css"){
							$dir = public_path()."/css/";
							copy("zip://".$file_path."#".$path, $dir.substr(strrchr(zip_entry_name($zip_entry), '/'), 1));
						}
						else if($newpath == "assets/images"){
							$dir = public_path()."/images/";
							copy("zip://".$file_path."#".$path, $dir.substr(strrchr(zip_entry_name($zip_entry), '/'), 1));
						}
						else if($newpath == "Models"){
							$dir = app_path().'/';
							copy("zip://".$file_path."#".$path, $dir.substr(strrchr(zip_entry_name($zip_entry), '/'), 1));
						}
						else if($newpath == "Views/_partial"){
							$dir = public_path()."/resources/views/_partial/";
							copy("zip://".$file_path."#".$path, $dir.substr(strrchr(zip_entry_name($zip_entry), '/'), 1));
						}
						else if($newpath == "Views"){
							$dir = resource_path()."/views/plugs/";
							copy("zip://".$file_path."#".$path, $dir.substr(strrchr(zip_entry_name($zip_entry), '/'), 1));
						}
						else if($newpath == "Routes"){
							$dir = app_path()."/Http/";
							$txt = file_get_contents("zip://".$file_path."#".$path);
							$fp = fopen($dir.'routes.php',"a");
							fwrite($fp, $txt);
							fclose($fp);
							break;
						}
						else if($newpath == "Migrations"){
							$dir = database_path()."/plug_migrations/";
							copy("zip://".$file_path."#".$path, $dir.substr(strrchr(zip_entry_name($zip_entry), '/'), 1));
							Artisan::call('migrate', ['--path'=> "database/plug_migrations"]);
							
							foreach (glob($dir."*.php") as $filename) {
							    $file = realpath($filename);
							    rename($file, str_replace(".php",".bkp",$file));
							}
						}
					}
				}
			}
		}
		return view('plugin.index')->with('message',"Successfully Extracted");
	}
}
