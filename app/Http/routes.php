<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('plugin');
});

Route::post('plugin/check', ['as' => 'plugin.check', 'uses' => 'PluginController@check_plugin']);
Route::post('plugin/install', ['as' => 'plugin.install', 'uses' => 'PluginController@install']);

Route::resource('plugin', 'PluginController');
Route::resource('plugin1', 'PluginController@index');
