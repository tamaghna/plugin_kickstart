@extends('layouts.app')
@section('content')
{{-- /*dd($plugin_details['plugin']['data'])/* --}}
<div class="mui-appbar">
	<!-- content -->
	<div class="title mui--text-display1 mui--text-left">
		Plugin Installation of {{ $plugin_details['plugin']['title'] }}
	</div>
</div>
<div class="mui-container m-640">
	<form action="{{ url('plugin/install') }}" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input type="hidden" name="plugin_path" value="{{$plugin_path}}"/>
		
		<div class="mui-panel mui--z3">

			<div class="mui--text-headline mui--text-dark-secondary plugin-header">
				<div class="mui--text-left plugin-title left">
					{{ $plugin_details['plugin']['title'] }}
				</div>
				<div class="mui--text-left plugin-icon">
					<img src="data:image/jpg;charset=utf8;base64,{{$plugin_icons}}"/>
				</div>
			</div>
			<div class="mui-divider"></div>
			<div class="mui--text-title clear plugin-description mui--text-body2">
				@foreach($plugin_details['plugin']['data'] as $key)
				<p>
					<i class="fa fa-file-text mui--text-accent-red"></i>
					<span class="mui--text-dark-hint"> Version: </span>{{$key['version']}}
				</p>
				<p>
					<i class="fa fa-hand-o-right mui--text-accent-red"></i>
					<span class="mui--text-dark-hint"> Description: </span>{{$key['description']}}
				</p>
				
				<input type="hidden" name="unique_id" value="{{base64_encode($key['unique_id'])}}" />
				
				<p>
					<i class="fa fa-code-fork mui--text-accent-red"></i>
					<span class="mui--text-dark-hint"> Author: </span>{{$key['author']}}
				</p>
				<p>
					<i class="fa fa-coffee mui--text-accent-red"></i>
					<span class="mui--text-dark-hint"> License: </span>{{$key['license']}}
				</p>
				<p>
					<i class="fa fa-calendar mui--text-accent-red"></i>
					<span class="mui--text-dark-hint"> Released On: </span>{{ date("F j, Y", strtotime($key['released_on']))}}
				</p>
				<p>
					<i class="fa fa-commenting mui--text-accent-red"></i>
					<span class="mui--text-dark-hint"> Support Link: </span>
					<a target="_blank" class="pointer mui--text-blue" href="{{$key['support_link']}}"> {{$key['support_link']}} </a>
				</p>
				@endforeach
			</div>
			<button type="submit" class="mui-btn mui-btn--raised mui-btn--primary right">
				Install
			</button>
		</div>
	</form>
</div>
@endsection
