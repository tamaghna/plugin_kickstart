@extends('layouts.app')
@section('content')
<div class="mui-appbar">
	<!-- content -->
	<div class="title mui--text-display1 mui--text-left">
		Plugin Demo
	</div>
</div>
<div class="mui-container m-640">
	<div class="mui-panel mui--z2">
		<form action="{{ url('plugin/check') }}" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
			<legend>
				Upload a plugin
			</legend>
			<input class="mui-btn mui-btn--small mui-btn--primary" type="file" value="Button" name="plugin" />
			<button type="submit" class="mui-btn mui-btn--danger">
				Upload
			</button>
		</form>
	</div>
</div>
@endsection
